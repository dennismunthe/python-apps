FROM python:latest
LABEL maintainer='dennismunthe'

WORKDIR 'usr/app/c=src'
COPY app.py ./

EXPOSE 8080
CMD ['python', '-m', 'flask', 'run']